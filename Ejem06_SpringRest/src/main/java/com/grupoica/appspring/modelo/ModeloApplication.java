package com.grupoica.appspring.modelo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//Decorador que permite incluir funcionalidad a las propiedades, clases ... 
@SpringBootApplication
public class ModeloApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModeloApplication.class, args);
		System.out.println("***************************");
		System.out.println("****** Mi app Spring ******");
		System.out.println("***************************");
		
	}

}
