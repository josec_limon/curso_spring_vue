package com.grupoica.appspring.modelo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.grupoica.appspring.modelo.daos.HeroesDAO;
import com.grupoica.appspring.modelo.entidades.Heroe;

@RestController
@RequestMapping("/api/heroes/")
@CrossOrigin
public class ControladorHeroes {

	@Autowired
	HeroesDAO heroesDAO;
	//private List<Heroe> misHeroes = new ArrayList<Heroe>();
	
	//@GetMapping("prueba")
	@RequestMapping(method=RequestMethod.GET, value="prueba")
	public Heroe leerHeroe() {
		System.out.println("Peticion recibida");
		Heroe heroe = new Heroe();
		heroe.setNombre("Hulk");
		
		return heroe;
	}
	
	@PostMapping
	public Heroe guardarHeroe(@RequestBody Heroe nuevoHeroe) {
		System.out.println("Heroe recibido " + nuevoHeroe.getNombre() );
		//misHeroes.add(nuevoHeroe);
		return heroesDAO.save(nuevoHeroe);
		
	}
	
	@PutMapping
	public Heroe actualizarHeroe(@RequestBody Heroe nuevoHeroe) {
		if(nuevoHeroe.getId() > 0) {
			System.out.println("Heroe recibido " + nuevoHeroe.getNombre() );
			return 	heroesDAO.save(nuevoHeroe);
		}
		else {
			System.out.println("Heroe sin ID " + nuevoHeroe.getNombre() );
			return null;
		}	
	}
	
	@DeleteMapping
	public Heroe borrarHeroe(@RequestBody Heroe borrarHeroe) {
		if(borrarHeroe.getId() > 0) {
			System.out.println("Heroe recibido " + borrarHeroe.getNombre() );
			heroesDAO.delete(borrarHeroe);
			return 	borrarHeroe;
		}
		else {
			System.out.println("Heroe sin ID " + borrarHeroe.getNombre() );
			return null;
		}	
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.DELETE)
	public void borrarHeroe(@PathVariable int id) {
		if(id > 0) {
			System.out.println("Heroe borrado id " + id);
			heroesDAO.deleteById(id);
		}
		else {
			System.out.println("Heroe no borrado sin ID " + id );
		}	
	}
	
	
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public Heroe getUno(@PathVariable int id) {
		if(id > 0) {
			System.out.println("Heroe borrado id " + id);
			return heroesDAO.getOne(id);
		}
		else {
			System.out.println("Heroe no borrado sin ID " + id );
			return null;
		}	
	}
	
	@RequestMapping(method=RequestMethod.GET, value="listado")
	public List<Heroe> getAll() {
		return heroesDAO.findAll();
	}
	
}
