package com.grupoica.ejem05modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        DerbyDBUsuario m = new DerbyDBUsuario();
        ArrayList<Usuario> listadoUsu = m.listar();
        
        for(Usuario usuario : listadoUsu) {
        	System.out.println("Usuario: " + usuario.getNombre());
        }
    }
}
