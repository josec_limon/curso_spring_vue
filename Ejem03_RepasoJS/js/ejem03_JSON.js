'use strict'

//JSON JavaScript Object Notation
// Otra forma de crear objetos con la notacion de JSON
// Al abrir y cerrar llaves se crea new Object() 

/*
let formaPago ={
    "modo":"Tarjeta credito",
    "comision": 2,
    "activa": true,
    "preparacion": null,
    "clientes":["Santander","Sabadell","BBVA", [1,23,55]], 
    "configuracion" : {
        "conexion": "ssl",
        "latencia": 15,
    }
};*/
//Leer de disco en el JSON y DES-SERIALIZARLO.
let formaPago = JSON.parse(window.localStorage.getItem("datos-forma-pago"));

let array =[]; //Array vacio == new Array();
let datos = ["Churros","Meninas",200,true,null,{"ale": "más datos"}];

//Array multidimensional.
let matriz =[
    [4,6,8],
    [3,7,7],
    [1,5,7]
];

//Añadir campo nuevo al JSON
formaPago.servidor = "http://visa.com";
formaPago["oculta"] ="Dame 5 centimos para mi";

//Acceso a alguno de estos datos.
document.write(`<br
<p>${formaPago.modo} - ${formaPago.clientes[1]} - ${formaPago.clientes[3][2]}</p>`);

document.write(`<h2>${JSON.stringify(formaPago)}</h2><br/>
<p>Usando forma HashMap: ${formaPago["servidor"]}</p>`);

alert(JSON.stringify(formaPago, null, 20));
//Guardar en el disco el JSON en el espacio total 
window.localStorage.setItem("datos-forma-pago", JSON.stringify(formaPago, null, 3));

let petUsu = prompt("¿Qué dato quieres?");
document.write(`<br> ${formaPago[petUsu]}` );
// SERIALIZAR: Convertir un objeto o estructura en memoria en un formato transmitible 
// ( o para enviar por red o para guardar en un fichero) y el formato puede ser texto (XML, JSON, YAML, o uno propio)
// formato binario, o encriptado.

let frutas = `[
     {"nombre": "pera", "precio": 20},
     {"nombre": "kiwi", "precio": 27},
     {"nombre": "fresa", "precio": 30}
]`;

// Parsear es la forma coloquial de decir "interpretrar o leer un texto"
// puede ser convertir un texto en otro texto, o en este caso, convertir 
// un texto en un objeto de memoria.
// Cuando hablamos del último caso la definición técnica es "DES-SERIALIZAR"

let objFrutas = JSON.parse(frutas);
document.write(`<br> ${objFrutas[1]["nombre"]} - ${objFrutas[2]["precio"]}`);