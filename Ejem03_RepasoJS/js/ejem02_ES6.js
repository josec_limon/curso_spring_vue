'use strict'

const PI = 3.1415926;

let unaVar = 20;
let unTexto = "Que pasa";

document.write(`<br>
Texto en varias
lineas, y además podemos mostrar <br>
variables asi: ${unaVar} y otro texto: ${unTexto}` + "<br>" +"<br>");


var suma = (x,y)=>  x+y;
var cuadrado = x => x**x;

document.write("Suma de 3 + 2 = " + suma(3,2) + "<br>");
document.write("Cuadrado de 3 = " + cuadrado(3) + "<br>");


// Clases en JS
class Dato{
    //Solo se puede tener un constructor
    constructor(x,y){
        this.x =x;
        this.y =y;
    }
    mostrar(){
        document.write(`<br> DATO:  x=${this.x}  y=${this.y}`);
    }
}

class Info extends Dato{
    constructor(x,y=20, z=20){
        super();
        this.x = x;
        this.y = y;
        this.z = z;
    }
    mostrar(){
        super.mostrar();
        document.write(` z=${this.z}`);
    }
}

let dato = new Dato("Algún dato", true);
dato.mostrar();

let info = new Info("Otra info");
info.mostrar();


