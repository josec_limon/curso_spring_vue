//Creamos un componente estatico de vue
Vue.component("app-saludo",{
    "template": `<h2>Hola soy un componente</h2>`
});

Vue.component("app-bloque",{
    "template": '<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>'
});

Vue.component("app-despedida",{
    "template": `<div><h2>A la voluntad de Dios</h2>
            <h2 class="titulo">Pues eso hasta luego!</h2>
            </div>`,
    style: `.titulo {background-color:red}` 
});

//Selecciona en el HTML el selector #app-section y coloca el template
new Vue(
    {
        "el": "#app-section",   
        template:  `<div><h2>Hola ICA</h2>
        <app-saludo></app-saludo></div>
        `
    }
);

new Vue(
    {
        "el": "#app-section-2",   
    }
);

new Vue({
    "el": "#app-section-3",
    template: `<app-bloque></app-bloque>`
});

