package com.grupoica.repasojava;

public class EjemploLambdas {
	
	//Una interfaz es FUNCIONAL cuando solo tiene un m�todo, eso significa que se le puede asignar una unica funcion.
	interface NuestraFun {
		//Solo puede ser un unico metodo. Tampoco si hubiera un parametro.
		float operacion(float v, float w);
	}

	public static float sumar(float x, float y){
		return x+y;
	}
	
	//Funcion Resta est�tica
	public static float restar(float x, float y){
		return x-y;
	}
	
	public static float[] sumarArrays(float[] arr_x, float[] arr_y){

		float[] arr_res = new float[arr_x.length];
		
		for(int i=0; i < arr_x.length && i < arr_y.length; i++ ){
			arr_res[i] = arr_x[i] + arr_y[i];
		}
		return arr_res;
	}
	
	public static float[] operarArrays(float[] arr_x, float[] arr_y, NuestraFun funCallBack){

		float[] arr_res = new float[arr_x.length];
		
		for(int i=0; i < arr_x.length && i < arr_y.length; i++ ){
			arr_res[i] = funCallBack.operacion(arr_x[i] , arr_y[i]);
		}
		return arr_res;
	}
	
	public static void ejecutarLambdas() {
		
		float[] a = { 2,3,4};
		float[] b = { 20,30,40};
		//float[] result=sumarArrays(a,b);
		
		//mostrarArray(result);
		 
		 System.out.println("\n");
		 float[] result2=operarArrays(a,b, EjemploLambdas::sumar);
		 
		 mostrarArray(result2);
		 System.out.println("\n");
		 
		 // Dos calculos sobre arrays:
		 // Resta usando fun est�tica y divisi�n usando lambda
		 
		 System.out.println("Resta usando fun est�tica y divisi�n usando lambda \n");
		 float[] result3=operarArrays(a,b, EjemploLambdas::restar);
		 
		 mostrarArray(result3);
		 
		 System.out.println("\n");
		 float[] result4=operarArrays(a,b, (float x, float y) -> {
			 //System.out.println("Multiplicando: " + x + " * " + y + " = " + x*y );
			 return x*y;
		 });
		 
		 mostrarArray(result4);
		 
		 System.out.println("\n");
		 System.out.println("Divisi�n usando lambda \n");
			
		 float[] result5=operarArrays(a,b, (float x, float y) -> {
			 //System.out.println("Dividir: " + x + " / " + y + " = " + x/y );
			 return x/y;
		 });
		 
		 mostrarArray(result5);	 
		
	}
	
	public static void mostrarArray(float [] arr) {
		 for(float f : arr){
			  System.out.print(" [ " + f  + " ] ");
			  }
		 
			/*for(int i=0; i< result.length; i++) {
			System.out.print("Array ["+ i + "] = " + result[i] );
		}
		*/

	}

}
