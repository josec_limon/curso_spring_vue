package com.grupoica.repasojava;

import com.grupoica.repasojava.interfaces.ProbandoVehiculos;
import com.grupoica.repasojava.modelo.GestionUsuarios;
import com.grupoica.repasojava.modelo.Usuario;
import com.grupoica.repasojava.modelo.Loco;

public class ProgramaMain {

	//Programacion Orientada a Objetos
	/*
	 * La unidad basica de almacenamiento son los tipos primitivos y los objetos que
	 * est�n basados en clases. Las clases son el molde, plantilla, estructura que 
	 * indica c�mo ser�n todos los objetos instanciados a partir de ella, es decir, 
	 * sus variables miembro (campos, atrivutos, propiedades... ) y sus m�todos (funciones propias). 
	 * 
	 * Las caracter�sticas de la POO:
	 * 	- Herencia:
	 * 	- Encapsulaci�n:
	 * 	- Polimorfismo: 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploLambdas.ejecutarLambdas();
		//ProbandoVehiculos.probar();
	/*	
		GestionUsuarios gesUsu = new GestionUsuarios();
		//gesUsu.listaUsuarios.add("A�ado nuevo");
		//gesUsu.addElemento("adasd");
		//gesUsu.listarUsuarios();
		
		//EjemploMemoria.pruebaPasoPorValor();
		//EjemploMemoria.pruebaPasoPorReferencia();
		
		//Casting (conversion de un tipo de dato a otro)
		// Casting explicito: aposta, adrede, con (tipo) por delante
		//Perdemos los decimales. 
		//double doble = 10.43343;
		//int entero = (int) Math.floor(doble);
		

		//Esto nos lo llevamos al main y realizar el metodo modificar usuario.
		//this.listaUsuarios.add(10);
		Usuario usu = new Usuario();
		
		//Modificacion dia 2 GETTERS AND SETTERS
		//usu.edad=30;
		//usu.nombre="Fulanito Mengano";
		usu.setEdad(30);
		usu.setNombre("Fulanito Mengano");
		
		//System.out.println("Nombre: " + usu.getNombre());
		// Si listaUsuarios es un ArrayList sin tipo, es que su tipo es Object, 
		// cada uno de los elementos es object. Por lo tanteo hacemos un casting 
		// implicito (sin par�ntesis) de usu a object gracias al polimorfismo. 
		gesUsu.addUsuario(usu);
		
		//Modificacion dia 2 GETTERS AND SETTERS
		Usuario usuario = new Usuario("U2",50);
		//System.out.println("Edad U2: "  + usuario.edad);
		System.out.println("Edad U2: "  + usuario.getEdad());
		//this.listaUsuarios.add(usu2);
		gesUsu.addUsuario(usuario);
		gesUsu.addUsuario("Juan", 25);
		
		//this.listaUsuarios.add("Texto");
		//this.listaUsuarios.add(new Object());
		//----------------------------------------------------------
		
		//Comparamos ambos usuarios, utiliza los metodos sobrescritos en la clase
		if(usuario.equals(usu)) {
			System.out.println("Son iguales.");
		}
		else 
		{
			System.out.println("Son distintos.");
		}
		
		Loco joker1 = new Loco();
		System.out.println("Joker: " + joker1.getNombre());
		joker1.setNombre("Joker");
		joker1.setTipoLocura(true);
		if(joker1.isTipoLocura()) {
			System.out.println("Joker esta loco:" + joker1.toString());
		}else {
			System.out.println("Joker no esta loco:" + joker1.toString());
		}
		
		System.out.println("Joker: " + joker1.toString());
		
		//this.listaUsuarios.add(joker);
		
		
		//System.out.println("Listado de usuarios");
		//this.mostrarUsuarios("Loco");
		
		//System.out.println("Encontrar usuario");
		//obtenerUsuario("Fulanito Mengano");
		System.out.println();
		// Ejercicios
		System.out.println("------------------- EJERCICIOS --------------------------" );
		// Ejercicio 1: Mover todo lo del constructor de Gestion USUARIOS. Crear los usuarios
		// Creamos los usuarios
		Usuario usu1 = new Usuario("Fulanito Mengano", 25);
		Usuario usu2 = new Usuario("Paco Porras", 50);
		Usuario usu3 = new Usuario("Eustaquio Trompa", 15);
		Usuario usu4 = new Usuario("Evaristo Garc�a", 15);
		
		// Creamos el loco
		Loco joker = new Loco();
		joker.setNombre("Joker");
		joker.setEdad(35);
		joker.setTipoLocura(true);
				
		// Utilizamos el metodo de Gestion Usuario para a�adirlos
		gesUsu.addUsuario(usu1);
		gesUsu.addUsuario(usu2);
		gesUsu.addUsuario(usu3);
		gesUsu.addUsuario(usu4);
		gesUsu.addUsuario(joker);
		
		//Mostrar los usuarios del listado
		EjemploHashMap.probandoHashMap();
		
		gesUsu.listarUsuarios();
		
		System.out.println("-----------------------------------------------------------------" );
		// Ejercicio 2: Modificar usuario
		System.out.println("Modificar el usuario con nombre Fulanito Mengano por nombre Fulanito Metepatas " );
		gesUsu.modificarNombreUsuario("Fulanito Mengano", "Fulanito Metepatas");
		gesUsu.listarUsuarios();
		
		System.out.println("-----------------------------------------------------------------" );
		System.out.println("Cambiar la edad del usuario con nombre Fulanito Metepatas por 46 ");
		gesUsu.modificarEdadUsuario("Fulanito Metepatas", 46);
		gesUsu.listarUsuarios();
		
		/* Se sobrecargan los metodos y no se ponen nuevos nombres al metodo, solo admitiendo parametros
		 * 
		 * gesUsu.modificar("Juan", "Juan Carlos");
		 * gesUsu.modificar("Juan Carlos", "Juan Carlos I", 50);
		 * gesUsu.modificar("Juan Carlos I", 80);	
		 * 
		 * */
		
	
	/*
		
		// Ejercicio 4: Filtrar usuario por edad (mostrar todos los que tengan cierta edad)
		System.out.println("-----------------------------------------------------------------" );
		System.out.println("Mostrar usuarios por una edad concreta - 15");	
		if(gesUsu.buscarUsuarioEdad(15)) {
			System.out.println("No se ha encontrado ningun usuario con esta edad");
		}
		
		// Ejercicio 5: Filtrar usuarios por edad (los que esten en un rango de edad)
		System.out.println("-----------------------------------------------------------------" );
		System.out.println("Mostrar usuarios por un rango de edad - 20 a 50");	
		if(gesUsu.buscarUsuarioEdad(20,50)) {
			System.out.println("No se ha encontrado ningun usuario en este rango de edad.");
		}
		// Ejercicio 3: Eliminar un usuario
		System.out.println("-----------------------------------------------------------------" );
		System.out.println("Eliminar un usuario ");
		gesUsu.eliminarUsuario("Joker");
		
		gesUsu.listarUsuarios();
		
	/*	// Ejercicio 3: Eliminar todos los usuarios
		System.out.println("-----------------------------------------------------------------" );
		System.out.println("Eliminar todos los usuarios ");
		boolean vacio= gesUsu.eliminarTodosUsuarios();
		
		if(vacio) {
			System.out.println("El listado esta vacio");
		}else {
			System.out.println("Quedan usuarios en el listado");
		}
		gesUsu.listarUsuarios();
		*/
	
		
	}

}
