package com.grupoica.repasojava.interfaces;

public class Caballo extends Vehiculo implements Animal{

	//Clase Caballo, con la propiedad raza y que herede de vehiculo
	// Variable de clase y en caso de llevar static seria variable global
	int dientes;
	
	public Caballo(String marca, float peso, int dientes) 
	{
		super(marca,peso);
		this.dientes=dientes;
	}

	public int getDientes() {
		return dientes;
	}

	public void setDientes(int dientes) {
		this.dientes = dientes;
	}

	@Override
	public void aceleracion() {
		//super.aceleracion();
		System.out.println( marca + " - Coz inminente, cuidado! " );
	}

	@Override
	public void desplazarse(float distancia) {
		System.out.println( marca + " galopan " + distancia + " metros. " );
		
	}

	@Override
	public void alimentarse(String comida) {
		if(comida.contains("calabacin")) {
			System.out.println("Come sano " + comida);
		}else {
			System.out.println("Hi hi hi quiero calabacines!. No quiero "+ comida);
		}
		
	//	System.out.println("El caballo se alimenta de " + comida);	
	}
	

	@Override
	public String toString() {
		return "Caballo [marca=" + marca + ", peso=" + peso + ", dientes=" + dientes + "]";
	}
	
	
}
