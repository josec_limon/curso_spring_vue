package com.grupoica.repasojava.interfaces;

public class Perro implements Animal{
	
	protected String nombre;
	protected String raza;
	protected int edad;
	
	public Perro(String nom, String raza, int edad) {
		this.edad=edad;
		this.nombre=nom;
		this.raza=raza;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public void alimentarse(String comida) {
		if(comida.contains("chuleton")) {
			System.out.println("Come sano " + comida);
		}else {
			System.out.println("Hi hi hi quiero chuleton!. No quiero "+ comida);
		}
		
		//System.out.println("El perro se alimenta de " + comida);	
	}

	@Override
	public String toString() {
		return "Perro [nombre=" + nombre + ", raza=" + raza + ", edad=" + edad + "]";
	}
	
	
	
}
