package com.grupoica.repasojava.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProbandoVehiculos {

	public static void probar() {		
		Coche miCoche = new Coche("Kia", 1500, 60.34f);
		miCoche.aceleracion();
		
		Coche miCocheFines = new Coche("Hammer", 2500, 60.34f);
		miCocheFines.aceleracion();

		Caballo miCorcel = new Caballo("Pegaso", 15f, 40 );
		miCorcel.aceleracion();
		
		
		//Polimorfismo : pero lo que se pasa es la referencia
		Vehiculo unVehiculo = miCoche;  //Casting implicito
		Object unObjeto = miCoche;
		
		Coche unCoche = (Coche) unObjeto; //Casting explicito
		System.out.println(unObjeto.toString());
		unVehiculo.aceleracion();
		
		/*ArrayList<Vehiculo> garaje = new ArrayList<>();
		garaje.add(miCorcel);
		garaje.add(miCoche);
		garaje.add(miCocheFines);
		//No se puede 
		//garaje.add(new Vehiculo("Que no me he comprado", 30));
		garaje.add(unVehiculo);
		
		for(Vehiculo vehiculo :garaje) {
			vehiculo.aceleracion();
			vehiculo.desplazarse(1.5f);
		}*/
		
		//Declarar interfaz y se le iguala 
		Motorizable vehMotor = miCoche;
		System.out.println("\n");
				
		System.out.println("------------------- Ejercicios ---------------------");
		
		//Ejercicios I
		// 
		// 1- Garaje ser� solo para objetos motorizables
		System.out.println("Ejercicio 1- Garaje ser� solo para objetos motorizables");
		ArrayList<Motorizable> garaje = new ArrayList<>();
		//garaje.add(miCorcel);
		garaje.add(miCoche);
		garaje.add(miCocheFines);
		garaje.add((Coche)unVehiculo);
		
		for(Motorizable objMotor :garaje) {
			objMotor.encender();
			Vehiculo vehiculo =(Vehiculo) objMotor;
			vehiculo.aceleracion();
			vehiculo.desplazarse(1.5f);
		}
		System.out.println("\n");
		
		// 2- Crear clase Patinete que sea motorizable, pero no vehiculo
		
		System.out.println("Ejercicio 3- Guardaremos un patinete en el garaje");
		
		// 3- Guardaremos un patinete en el garaje.
				//vehMotor.garaje.add(new Patinete(2500, "Patin Volador"));
				garaje.add(new Patinete(2500, "Patin Volador"));
				
				for(Motorizable motor: garaje) {
					motor.encender();
					if(motor instanceof Vehiculo) {
						Vehiculo vehi = (Vehiculo) motor;
						vehi.aceleracion();
						vehi.desplazarse(12f);
					}
					System.out.println(motor.toString());	
				}
		
		System.out.println("\n");
		
		//Ejercicios II
		// 
		// 4- Hacer una clase Perro (que tampoco es un vehiculo)
		// 5- Crear una interfaz Animal con m�todo alimentarse(String comida)
		// 6- Perro y Caballo que sean animales, y hacer una granja y alimentarlos. 
	

		Perro miPerro = new Perro("Blue","Mestizo",2);
		Perro miPerra = new Perro("Tris","Mestizo",2);
		
		/*ArrayList<Animal> miGranja = new ArrayList<>();
		
		miGranja.add(miPerra);
		miGranja.add(miPerro);
		miGranja.add(miCorcel);
		
		for(Animal unAnimal: miGranja) {
			unAnimal.alimentarse("Pienso");
			System.out.println(unAnimal.toString());
			
		}*/
		
		System.out.println("Ejercicio 6- Perro y Caballo que sean animales, y hacer una granja y alimentarlos.");
		HashMap<String, Animal> granjaEscuela = new HashMap<String, Animal>();
		granjaEscuela.put("perra",miPerra);
		granjaEscuela.put("perro",miPerro);
		granjaEscuela.put("caballo",miCorcel);
		granjaEscuela.put("unicornio",new Caballo("Sangre",200,50));
		
		for(Map.Entry<String, Animal> unAnimal: granjaEscuela.entrySet()) {
			unAnimal.getValue().alimentarse("pollo");
			unAnimal.getValue().alimentarse("calabacin");
			unAnimal.getValue().alimentarse("chuleton");
			System.out.println("----");
			
		}
	}
}
