package com.grupoica.repasojava.interfaces;

// 2- Crear clase Patinete que sea motorizable, pero no vehiculo
public class Patinete implements Motorizable{

	protected int bateria;
	protected String modelo;

	public Patinete(int bateria, String modelo) {
		super();
		this.bateria = bateria;
		this.modelo = modelo;
	}


	public int getBateria() {
		return bateria;
	}


	public void setBateria(int bateria) {
		this.bateria = bateria;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	@Override
	public void encender() {
		this.bateria -= 2f;
		System.out.println( "El patinete " + this.modelo + " con bateria de " + this.bateria + " esta encendido. ");
		
	}


	@Override
	public String toString() {
		return "Patinete [bateria=" + bateria + ", modelo=" + modelo + "]";
	}
	
	
	


}
