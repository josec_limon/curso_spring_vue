package com.grupoica.repasojava.interfaces;

// En el caso que no queremos que sea posible instanciar una clase, la declaramos abstracta
// public abstract class Vehiculo
//Poniendo las clases abstractas hacen que las clases hijas tienen que implementar.

public abstract class Vehiculo {
	
	protected String marca;
	protected float peso;
	
	public Vehiculo(String marca, float peso) {
		this.marca=marca;
		this.peso=peso;
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	
	public void aceleracion() {
		System.out.println(getClass().getSimpleName() 
				+ " "  + marca +  " Acelerando...");
	}

	//La clase debe de ser abstracta, 
	public abstract void desplazarse(float distancia);
	
}
