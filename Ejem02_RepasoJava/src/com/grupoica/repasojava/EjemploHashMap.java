package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploHashMap {

	// Un hashmap es una coleccion tipica de java, tambien dinamica 
	// y la forma de acceso de los elementos es por clave, tipicamente string, 
	// aunque puede ser de cualquier otro tipo
	
	// Un arraylist es una coleccion dinamica de elementos, al cual accedemos 
	// por su indice, que se pueden repetir.
	
	//Por si se quiere poner como clave el conjunto nombre-fecha del HashMap
	/*
	 * public class ClaveDicc() {
			String nombre;
			String fecha;
		}
	 */
	
	//public static HashMap<ClaveDicc, Usuario> diccUsuarios = new HashMap<>();;
	public static HashMap<String, Usuario> diccUsuarios = new HashMap<>();
	
	public static void probandoHashMap() {
				
		diccUsuarios.put("Luis",new Usuario("Luis",10));
		diccUsuarios.put("Ana",new Usuario("Ana",20));
		diccUsuarios.put("Luisa",new Usuario("Luisa",25));
		diccUsuarios.put("Marta",new Usuario("Marta",35));
		
		Scanner miScan = new Scanner(System.in);
		
		System.out.println("Introduce el usuario");
		String nombre= miScan.nextLine();
		System.out.println("El usuario es " + diccUsuarios.get(nombre));
		
	}
}
