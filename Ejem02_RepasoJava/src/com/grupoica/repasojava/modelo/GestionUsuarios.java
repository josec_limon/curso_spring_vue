package com.grupoica.repasojava.modelo;

import java.util.ArrayList;

import com.grupoica.repasojava.EjemploHashMap;

/*
 * Clase que se encargar� de las operaciones CRUD:  CREATE, READ, UPDATE, DELETE (Operaciones de alta, baja, modificaci�n y consulta) 
 * */

public class GestionUsuarios {
	
	
	// Los arrays en Java tienen elementos del mismo tipo siempre y con una longitud que tu determinas al iniciarlo. 
	// Lista de objetos en su forma ambigua, todos los elementos son object
	//private ArrayList listaUsuarios;
	//Lista en su forma gen�rica (todos los elementos son del mismo tipo o algun tipo de heredero
	private ArrayList<Usuario> listaUsuarios;
	
	public GestionUsuarios() {
		super();
		this.listaUsuarios = new ArrayList<Usuario>();
	}
	
	//Creamos un metodo que no devuelva nada que sea listar usuarios
	public void listarUsuarios() {
		System.out.println("Listar usuarios del Array");
		for(int i = 0 ; i < this.listaUsuarios.size(); i++) {	
			System.out.println(this.listaUsuarios.get(i));
		}
	}
	
	//A�adir usuarios
	public void addUsuario(Usuario nuevoUsuario){
			this.listaUsuarios.add(nuevoUsuario);
			EjemploHashMap.diccUsuarios.put(nuevoUsuario.getNombre(), nuevoUsuario);
	}
	
	public void addUsuario(String nombre, int edad) {
		Usuario nuevoUsu = new Usuario(nombre, edad);
		this.listaUsuarios.add(nuevoUsu);
		EjemploHashMap.diccUsuarios.put(nombre, nuevoUsu);
	}
	
	//Encontrar usuario
	public Usuario obtenerUsuario(String nombre) {
		Usuario usuarioEncontrado=null;
		for(Usuario usu : listaUsuarios) {
			if(usu.getNombre().equalsIgnoreCase(nombre)) {
				System.out.println("Encontrado:" + usu.getNombre());
				usuarioEncontrado= usu;
				break;
			}
			
		}
		return usuarioEncontrado;
	}
	
	
	
	
	//Listar usuarios
	public void mostrarUsuarios(String nombre) {
		for(Usuario usu : listaUsuarios) {
			// if (usuObj instanceof Usuario) {
			// Casting de objetos gracias al poliformismo
			// otra manera
			// Usuario usu = (Usuario) usuObj;
			// if(usuObj.getClass().equals(Usuario.class) || usuObj.getClass().equals(Loco.class)) 
			// {
				// Usuario usu= (Usuario) usuObj;  
				if(usu.getNombre().equals(nombre)) 
				{
					System.out.println("Nombre: " + usu.getNombre());
				}	
		}
	}

	public void modificarNombreUsuario(String nombre, String nombreCambiar) {
		for (Usuario usu: listaUsuarios) {
			if(usu.getNombre().equals(nombre)) {
				System.out.println("Encontrado el usuario: " + usu.getNombre() + " - " + usu.getEdad());
				usu.setNombre(nombreCambiar);
				break;
			}
		}
	}

	
	public void modificarEdadUsuario(String nombre, int edadNueva) {
		System.out.println("Edad: " +edadNueva);
		for (Usuario usu: listaUsuarios) {
			if(usu.getNombre().equals(nombre)) {
				System.out.println("Encontrado el usuario: " + usu.getNombre() + " - " + usu.getEdad());
				usu.setEdad(edadNueva);
				break;
			}
		}
	}
	
	//Modificar ambos campos
	public void modificar(String nombre, String nuevoNombre, int edad) {
		Usuario usuMofif = obtenerUsuario(nombre);
		if(usuMofif != null) {
			usuMofif.setNombre(nuevoNombre);
			usuMofif.setEdad(edad);
		}
	}
	
	public void modificar(String nombre, String nuevoNombre) {
		Usuario usuMofif = obtenerUsuario(nombre);
		if(usuMofif != null) {
			usuMofif.setNombre(nuevoNombre);
		}
	}
	
	public void modificar(String nombre, int nuevaEdad) {
		Usuario usuMofif = obtenerUsuario(nombre);
		if(usuMofif != null) {
			usuMofif.setEdad(nuevaEdad);
		}
	}
	
	
	public void eliminarUsuario(String nombre) {
		Usuario usuBorrar= null;
		System.out.println("Usuario a borrar: " + nombre);
		for (Usuario usu: listaUsuarios) {
			if(usu.getNombre().equals(nombre)) {
				System.out.println("Se ha encontrado al  : " + usu.getNombre() + ". Procedemos a eliminarlo. \n");
				usuBorrar=usu;
				break;
			}
		}
		
		this.listaUsuarios.remove(usuBorrar);
		
	}

	public boolean eliminarTodosUsuarios() {
		boolean eliminado=false;
		//this.listaUsuarios.clear();
		this.listaUsuarios.removeAll(listaUsuarios);
		
		if(this.listaUsuarios.size() == 0) {
			eliminado=true;
		}
		return eliminado;
		
	}

	public boolean buscarUsuarioEdad(int edadBuscar) {
		System.out.println("Edad: " + edadBuscar);
		boolean encontrados=true;
		for (Usuario usu: listaUsuarios) {
			if(usu.getEdad() == edadBuscar) {
				encontrados=false;
				System.out.println("El usuario : " + usu.getNombre() + " tiene  " + usu.getEdad() + " a�os.");
			}
		}
		return encontrados;
		
	}

	public boolean buscarUsuarioEdad(int edadX, int edadY) {
		System.out.println("Rango de edad: " + edadX + " - " + edadY);
		boolean encontrados=true;
		for (Usuario usu: listaUsuarios) {
			if(usu.getEdad() >= edadX && usu.getEdad() <= edadY) {
				encontrados=false;
				System.out.println("El usuario : " + usu.getNombre() + " tiene  " + usu.getEdad() + " a�os.");
			}
		}
		return encontrados;
	}
}
