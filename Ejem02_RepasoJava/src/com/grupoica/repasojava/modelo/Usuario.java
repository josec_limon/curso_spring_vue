package com.grupoica.repasojava.modelo;

//T�pica clase Plain OLD Java Object (POJO)
//Una clase con sus atributos, constructores, getters y setters se denomina POJO

public class Usuario {

	private int edad;	//VALORES POR DEFECTO: INT/DOUBLE/FLOAT => 0 ; STRING=NULL; BOOLEAN=FALSE
	private String nombre;
	
	public Usuario() {
		super();
		nombre="Sin nombre";
	}
	
	//Sobrecarga de constructores
	public Usuario(String nombre, int edad) {
		super();
		this.nombre=nombre;
		this.edad=edad;	
	}
	
	//Getters and Setters

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	//@Override Sobrecarga de m�todo (No overrride, no sobreescritura)	
	public boolean equals(Object obj) {
		Usuario usu = (Usuario) obj;
		return this.nombre == usu.nombre && this.edad == usu.edad;
	}
	
	//Override: dos metodos o constructores con el mismo nombre pero con distinta signatura (es decir, que los parametros son distintos)	
	public boolean equals(Usuario usuario){
		  	return this.nombre == usuario.nombre && this.edad == usuario.edad;
	}
	
	
	/*
	// @Override Sobrecarga de m�todo (No override, no sobreescritura)
	// Dos m�todos o constructores con el mismo nombre pero con 
	// distina signatura (es decir, que los par�metros son distintos)
		public boolean equals(Usuario usuario) {
			return this.nombre == usuario.nombre 
				&& this.edad == usuario.edad;
	}
	*/

	// OVERRIDE: Sobreescribir: Machacar el m�todo del padre
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		// Sobreexcribiendo el metodo to String
		return "Usuario: " + nombre + "["+ edad + "]";
	}
	
	
	
	
}
