package com.grupoica.repasojava.modelo;

public class Loco extends Usuario {
	// Creamos la clase y donde pone superclass, seleccionamos USUARIO
	// De esa manera hereda. 
	
	private boolean tipoLocura;
	
	//constructores
	public Loco() {
		//Al extender, llama al constructor del padre. 
		super();
	}
	
	public Loco(String nombre, int edad, boolean tl) {
		super(nombre, edad);
		this.tipoLocura=tl;
	}

	//Getters and Setters
	public boolean isTipoLocura() {
		return tipoLocura;
	}

	public void setTipoLocura(boolean tipoLocura) {
		this.tipoLocura = tipoLocura;
	}

	
	public boolean equals(Object obj) {
		Usuario usu = (Usuario) obj;
		return super.getNombre() == usu.getNombre() && super.getEdad() == usu.getEdad();
	}
	
	public boolean equals(Usuario usuario){
		  	return super.getNombre() == usuario.getNombre() && super.getEdad() == usuario.getEdad();
	}
	
	//Metodo equals en loco
	/*
	public boolean equals(String nombre) {

		if(super.equals(nombre == super.getNombre())) {
			System.out.println("Los nombres coinciden");
			return true;
		}
		else {
			System.out.println("Los nombres son distintos");
			return false;
		}
	}


	@Override
	public boolean equals(int edad) {
		if(super.equals(edad == super.getEdad())) {
			System.out.println("Las edades coinciden");
			return true;
		}
		else {
			System.out.println("Las edades son distintas");
			return false;
		}
	}
*/


	//Metodo toString en loco
		@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Usuario: " + super.getNombre() + "[" + super.getEdad() + "]" + "[Loco = " + this.tipoLocura +  "]";
	}


	
	
}
