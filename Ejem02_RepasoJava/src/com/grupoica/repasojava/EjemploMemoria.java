package com.grupoica.repasojava;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploMemoria {
	
	public static void pruebaPasoPorValor() {
		int xx = 10;
		boolean y = true;
		String z = "Texto al declarar";
		funcionCualquiera(xx, y, z);
		System.out.println("XX = " + xx + " , Y = " + y + " , Z = "+ z);	
	}
	
	//Las variables primitivas (y String) se pasan por valor se crean copias de las variables
	private static void funcionCualquiera(int x, boolean y, String z) {
		
		System.out.println("X = " + x + " , Y = " + y + " , Z = "+ z);
		x=200;
		y=false;
		z= "Texto nuevo dentro de funci�n";
		System.out.println("X = " + x + " , Y = " + y + " , Z = "+ z);
			
	}
	
	//Se pasa el puntero de memoria (la referencia de memoria). Solo hace un duplicado de la direccion de memoria. 
	public static void pruebaPasoPorReferencia() {
		/*
		 * Declaracion variable: Reservamos un espacio peque�o solo para la direccion de memroia, es decir, 
		 * entre 4 y 8 bytes.
		 * 
		 * En realidad solo declaramos una referencia a un objeto �Que valor tiene?
		 * 
		 * */
		Usuario alguien = null; //Direccion 0, a ninguna parte
		//Instanciacion del objeto y asignacion a la variable
		//Instanciacion : reserva de memoria para todos los campos (4+8 bytes = 12 bytes)
		// Llamada al constructor: Se reservan otro 20 bytes (12 + 20 =32 bytes)
		// Asignacion coge la direcion de memoria que debuelve new y la pone en
		alguien = new Usuario("Pepito",30);
		int array[] = new int[3];
		
		array[0]=10;
		array[1]=20;
		array[2]=30;
		otraFuncion(alguien, array);
		
		System.out.println("Nombre = " + alguien.getNombre() + ", Elemento 0 = " + array[0]);
	
		//La variable es diferente pero apunta al mismo sitio, por lo tanto modifica al primero. 
		int otroArray[] = array;
		otroArray[0] =333;
		System.out.println("Nombre = " + alguien.getNombre() + ", Elemento 0 = " + array[0]);
		
	}
	
	private static void otraFuncion(Usuario parUsu, int[] parArr) {
		System.out.println("Nombre = " + parUsu.getNombre() + ", Elemento 0 = " + parArr[0]);
		
		parUsu.setNombre("Modif en funcion");
		parArr[0] = 9999;
		
		System.out.println("Nombre = " + parUsu.getNombre() + ", Elemento 0 = " + parArr[0]);
			
	}
}
