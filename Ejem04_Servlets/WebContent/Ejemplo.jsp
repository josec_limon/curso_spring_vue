<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>P�gina JSP</title>
	</head>
	<body>
		<h1>Ejemplo JSP: Es un servlet</h1>
		<!-- Para crear una funci�n -->
	<%! 
		String crearOL(int v)
		{
			String ol="<ol>";
			int i=v;
			while(i > 0){
				ol += "<li>Cuenta atras " + i + " / 5 </li>";
				i--;
			}
			return ol +="</ol>";
		} 
	%>
		<hr style="border:"2 px solid red">
		<% 
			Date d= new Date();
			out.println(d.toString());
			if(d.getSeconds() % 2 == 0) {
		%>		
			<p style="background-color:red">Prueba otra vez, hasta un segundo impar</p>
		<% } else { %>
				<p style="background-color:blue"></p>
				<%--Este comentario es como el /**/ de Java, no se envia al cliente --%>
				<!-- Los comentarios de HTML si se envian -->
				<%= crearOL(d.getSeconds())%>
				<hr/>
		<% } %>
	</body>
</html>