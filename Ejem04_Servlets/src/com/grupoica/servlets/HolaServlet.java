package com.grupoica.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/hola")
public class HolaServlet extends HttpServlet {
	//Un servlet es una pagina que se encarga de devolver y realizar peticiones GET y POST
	
	private static final long serialVersionUID = 1L;

    public HolaServlet() {
     super();
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String strNombre=req.getParameter("nombre");
		System.out.println("Parametro:" + strNombre);
		String html="<html><head><title>Formulario envio</title></html><body>";
		
		if(strNombre == null || "".equals(strNombre)) {
			html += "<h2>Pon el nombre</h2>";
		}
		else {
			html += "<form action='./hola.do' method='post'>" + 
			"Veces: <input type='number' name='veces' />" + 
			"<input type='submit' value='POST' /></form>";
		}
		html +="</body></html>";
		
		resp.getWriter().append(html);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//Ejercicio que repita tantas veces un texto que queramos
		
		
		int iVeces=0;
		
		try(PrintWriter escritor =resp.getWriter()) {
			String strVeces=req.getParameter("veces");
			escritor.println("<html><head><title>Formulario envio</title></html><body>");
			
			if(strVeces == null || "".equals(strVeces)){
				System.out.println("No ha puesto datos el usuario: " + strVeces);
				escritor.println("<h2>Falta n�mero de veces</h2>");
			}else {
				System.out.println("Usuario ha puesto " + strVeces + " veces");
				iVeces = Integer.parseInt(strVeces);
				escritor.println("<ul>");
				for (int i=1; i<=iVeces; i++) {
					String htmlVeces = "<li>Imprimiendo texto - Vez " + i +"</li>";
					escritor.println(htmlVeces);
					}
				escritor.println("</ul>");	
			}
			escritor.println("</body></html>");
	
		}catch(NumberFormatException ex) {
			resp.getWriter().append("<p>Debes de introducir un n�mero.<p>" + "<p><a href='./hola.do?nombre=Argumento+desde+url'>Volver al formulario</a></p> " );
		}
		
	}

}
